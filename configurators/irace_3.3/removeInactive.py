import os
import sys
sys.path.append('./configurators/irace_3.3/')
import pcsParser

pcs = pcsParser.PCS(sys.argv[1])
defaultFile = sys.argv[2]

with open(defaultFile) as f_in:
    lines = f_in.read().split('\n')

params = lines[0].split(' ')
values = lines[1].split(' ')
config = {}
for i in range(0,len(params)):
    t = pcs.getAttr(pcs.lookupParamID(params[i]),'type')
    if(t == 'real'):
        config[params[i]] = float(values[i])
    elif(t == 'integer'):
        config[params[i]] = int(values[i])
    else:
        config[params[i]] = values[i].strip()

reducedConfig = pcs.removeInactive(config)

os.system('mv ' + defaultFile + ' ' + defaultFile +'.with-inactive')

with open('paramfile.default.irace','w') as f_out:
    f_out.write(' '.join(params) + '\n')
    f_out.write(' '.join([str(reducedConfig[params[i]]) if params[i] in reducedConfig.keys() else 'NA' for i in range(0,len(params))]) + '\n')

        
